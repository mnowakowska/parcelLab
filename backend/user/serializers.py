from rest_framework import serializers

from user.models import Address


class AddressSerializer(serializers.ModelSerializer):

    class Meta:
        model = Address
        fields = ['street', 'zip_code', 'city', 'country']
