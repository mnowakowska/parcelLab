import factory

from django.contrib.auth.models import User

from order.models import Article, Order, OrderArticle
from tracking.models import Tracking, TrackingStatus, TrackingCheckpoint
from user.models import Address


class UserFactory(factory.django.DjangoModelFactory):
    id = factory.Sequence(lambda x: x)
    email = factory.Sequence(lambda n: 'user%03d@test.com' % n)

    class Meta:
        model = User


class AddressFactory(factory.django.DjangoModelFactory):
    user = factory.SubFactory(UserFactory)

    class Meta:
        model = Address


class OrderFactory(factory.django.DjangoModelFactory):
    id = factory.Sequence(lambda x: x)
    order_no = factory.Sequence(lambda x: x)
    user = factory.SubFactory(UserFactory)
    address = factory.SubFactory(AddressFactory)

    class Meta:
        model = Order


class TrackingFactory(factory.django.DjangoModelFactory):
    order = factory.SubFactory(OrderFactory)
    tracking_number = factory.Sequence(lambda x: x)

    class Meta:
        model = Tracking


class TrackingStatusFactory(factory.django.DjangoModelFactory):
    status = factory.Sequence(lambda x: x)
    status_text = factory.Faker('name')

    class Meta:
        model = TrackingStatus


class TrackingCheckpointFactory(factory.django.DjangoModelFactory):
    tracking = factory.SubFactory(TrackingFactory)
    status = factory.SubFactory(TrackingStatusFactory)
    timestamp = factory.Sequence(lambda x: x)

    class Meta:
        model = TrackingCheckpoint
