from datetime import datetime, timezone

import pytest

from rest_framework.test import APIClient

from tests.factories import (
    OrderFactory, TrackingFactory, TrackingCheckpointFactory, UserFactory)


@pytest.fixture()
def user():
    return UserFactory()


@pytest.fixture()
def order(user):
    return OrderFactory(user=user)


@pytest.fixture()
def order_other_user():
    return OrderFactory()


@pytest.fixture()
def tracking(order):
    return TrackingFactory(order=order)


@pytest.fixture()
def tracking_checkpoints(tracking):
    return [
        TrackingCheckpointFactory(tracking=tracking, timestamp=datetime(2020, 1, 1, tzinfo=timezone.utc)),
        TrackingCheckpointFactory(tracking=tracking, timestamp=datetime(2019, 1, 1, tzinfo=timezone.utc)),
        TrackingCheckpointFactory(tracking=tracking, timestamp=datetime(2022, 1, 1, tzinfo=timezone.utc)),
        TrackingCheckpointFactory(tracking=tracking, timestamp=datetime(2021, 1, 1, tzinfo=timezone.utc)),
    ]


@pytest.fixture()
def client():
    client = APIClient()
    return client
