from django.urls import reverse


def test_get_orders_no_user(db, client, order):
    url = reverse('order:order-list')
    response = client.get(url)
    assert response.status_code == 404


def test_get_orders_incorrect_user(db, client, order):
    url = reverse('order:order-list')
    response = client.get(url, {'user': 'incorrect@user.com'})
    assert response.status_code == 404


def test_get_orders(db, client, order, order_other_user):
    url = reverse('order:order-list')
    response = client.get(url, {'user': order.user.email})
    assert response.status_code == 200
    orders = response.json()
    assert len(orders) == 1
    assert orders[0]['id'] == order.id


def test_get_orders_last_status(db, client, order, tracking_checkpoints):
    url = reverse('order:order-list')
    response = client.get(url, {'user': order.user.email})
    assert response.status_code == 200
    order = response.json()[0]
    # 3rd checkpoint is the newest one
    assert order['newest_status'] == tracking_checkpoints[2].status.status_text
