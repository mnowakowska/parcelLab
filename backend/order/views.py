from django.contrib.auth.models import User
from django.db.models import OuterRef, Subquery
from django.shortcuts import get_object_or_404
from rest_framework import viewsets
from rest_framework.response import Response

from order.models import Order
from order.serializers import OrderDetailSerializer, OrderSerializer
from tracking.models import TrackingCheckpoint


class OrderViewSet(viewsets.ViewSet):
    """
    A simple ViewSet for listing or retrieving orders.
    """
    def list(self, request):
        user_email = request.GET.get('user')
        user = get_object_or_404(User, email=user_email)
        newest = TrackingCheckpoint.objects.filter(tracking=OuterRef('tracking__pk')).order_by('-timestamp')
        queryset = Order.objects.annotate(
            newest_status=Subquery(newest.values('status__status_text')[:1]),
        ).filter(user=user)
        serializer = OrderSerializer(queryset, context={'request': request}, many=True)
        return Response(serializer.data)

    def retrieve(self, request, pk=None):
        queryset = Order.objects.all()
        order = get_object_or_404(queryset, pk=pk)
        serializer = OrderDetailSerializer(order, context={'request': request})
        return Response(serializer.data)
