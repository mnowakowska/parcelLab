from rest_framework import serializers

from order.models import Article, Order, OrderArticle
from tracking.serializers import TrackingSerializer
from user.serializers import AddressSerializer


class ArticleSerializer(serializers.ModelSerializer):
    retailer = serializers.CharField(source='article.retailer')
    article_no = serializers.CharField(source='article.article_no')
    image_url = serializers.CharField(source='article.image_url')
    name = serializers.CharField(source='article.name')

    class Meta:
        model = OrderArticle
        fields = ['article_quantity', 'retailer', 'article_no', 'image_url', 'name']


class OrderSerializer(serializers.ModelSerializer):
    address = AddressSerializer()
    newest_status = serializers.CharField()
    order_url = serializers.HyperlinkedIdentityField(
        view_name='order:order-detail',
        read_only=True,
    )

    class Meta:
        model = Order
        fields = ['id', 'order_no', 'order_url', 'newest_status', 'address']


class OrderDetailSerializer(serializers.ModelSerializer):
    tracking = TrackingSerializer(source='tracking_set', many=True)
    address = AddressSerializer()
    articles = ArticleSerializer(source='orderarticle_set', many=True)

    class Meta:
        model = Order
        fields = ['id', 'order_no', 'address', 'tracking', 'articles']
