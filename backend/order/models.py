from django.contrib.auth.models import User
from django.db import models

from user.models import Address


class Order(models.Model):
    order_no = models.CharField(max_length=250, unique=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    address = models.ForeignKey(Address, on_delete=models.CASCADE)


class Article(models.Model):
    retailer = models.CharField(max_length=250)
    article_no = models.CharField(max_length=250)
    image_url = models.URLField(null=True, blank=True)
    name = models.CharField(max_length=250)

    class Meta:
        unique_together = [['retailer', 'article_no']]


class OrderArticle(models.Model):
    article = models.ForeignKey(Article, on_delete=models.CASCADE)
    order = models.ForeignKey(Order, on_delete=models.CASCADE)
    article_quantity = models.PositiveIntegerField(default=1)
