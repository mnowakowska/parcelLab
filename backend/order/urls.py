from django.urls import include, path
from rest_framework import routers

from order.views import OrderViewSet

app_name = 'order'

router = routers.DefaultRouter()
router.register(r'order', OrderViewSet, basename='order')

urlpatterns = [
    path('', include(router.urls)),
]
