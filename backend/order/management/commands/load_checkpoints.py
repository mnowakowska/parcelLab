import argparse
import csv

from django.core.management.base import BaseCommand

from tracking.models import Tracking, TrackingCheckpoint, TrackingStatus


class Command(BaseCommand):
    help = "Load tracking status update"

    def add_arguments(self, parser):
        parser.add_argument('--file', type=argparse.FileType('r'), required=True,
                            help='CSV file with checkpoints details')

    def add_tracking(self, tracking_number, location, timestamp, status, status_text,
             status_details):
        try:
            tracking = Tracking.objects.get(tracking_number=tracking_number)
        except Tracking.DoesNotExist:
            self.stdout(f'Could not find any order with tracking number: {tracking_number}')
            return
        tracking_status, _ = TrackingStatus.objects.get_or_create(
            status=status,
            defaults=dict(status_text=status_text, status_details=status_details))
        TrackingCheckpoint.objects.create(
            tracking=tracking, status=tracking_status,
            timestamp=timestamp, location=location)

    def handle(self, *args, **options):
        csv_file = options['file']
        csv_reader = csv.reader(csv_file, delimiter=';')
        next(csv_reader, None)  # skip headers
        for [tracking_number, location, timestamp, status, status_text,
             status_details] in csv_reader:
            self.add_tracking(tracking_number, location, timestamp, status, status_text, status_details)
