import argparse
import csv

from django.contrib.auth.models import User
from django.core.management.base import BaseCommand

from order.models import Article, Order, OrderArticle
from tracking.models import Tracking
from user.models import Address

PROVIDER = 'parcelFashion'


class Command(BaseCommand):
    help = "Load orders from parcelFashion provider"

    def add_arguments(self, parser):
        parser.add_argument('--file', type=argparse.FileType('r'), required=True,
                            help='CSV file with order details')

    def add_article(self, article_no, image_url, product_name, quantity, order):
        if not article_no:
            return

        article, _ = Article.objects.get_or_create(
            retailer=PROVIDER, article_no=article_no,
            defaults=dict(image_url=image_url, name=product_name))
        OrderArticle.objects.create(article=article, order=order, article_quantity=quantity)

    def add_tracking(self, tracking_number, courier, order):
        Tracking.objects.get_or_create(
            tracking_number=tracking_number,
            defaults=dict(order=order, courier=courier))

    def handle(self, *args, **options):
        csv_file = options['file']
        csv_reader = csv.reader(csv_file, delimiter=';')
        next(csv_reader, None)  # skip headers
        for [order_no, tracking_number, courier, street, zip_code,
             city, destination_country, email, article_no,
             image_url, quantity, product_name] in csv_reader:
            user, _ = User.objects.get_or_create(email=email)
            address, _ = Address.objects.get_or_create(
                user=user, street=street, zip_code=zip_code,
                city=city, country=destination_country)
            order, _ = Order.objects.get_or_create(
                order_no=order_no, defaults=dict(user=user, address=address))
            self.add_article(article_no, image_url, product_name, quantity, order)
            self.add_tracking(tracking_number, courier, order)
