from rest_framework import serializers

from tracking.models import Tracking, TrackingStatus, TrackingCheckpoint


class TrackingStatusSerializer(serializers.ModelSerializer):

    class Meta:
        model = TrackingStatus
        fields = ['status_text', 'status_details']


class TrackingCheckpointSerializer(serializers.ModelSerializer):
    status = TrackingStatusSerializer()
    timestamp = serializers.DateTimeField(format="%d-%m-%Y %H:%M:%S")

    class Meta:
        model = TrackingCheckpoint
        fields = ['status', 'timestamp', 'location']


class TrackingSerializer(serializers.ModelSerializer):
    tracking_checkpoints = TrackingCheckpointSerializer(source='trackingcheckpoint_set', many=True)

    class Meta:
        model = Tracking
        fields = ['tracking_number', 'tracking_checkpoints']
