from django.db import models

from order.models import Order


class Tracking(models.Model):
    order = models.ForeignKey(Order, on_delete=models.CASCADE)
    courier = models.CharField(max_length=250)
    tracking_number = models.CharField(max_length=250)


class TrackingStatus(models.Model):
    status = models.CharField(max_length=250, unique=True)
    status_text = models.CharField(max_length=250)
    status_details = models.TextField(max_length=250)


class TrackingCheckpoint(models.Model):
    tracking = models.ForeignKey(Tracking, on_delete=models.CASCADE)
    status = models.ForeignKey(TrackingStatus, on_delete=models.CASCADE)
    timestamp = models.DateTimeField()
    location = models.CharField(max_length=250, blank=True)

    class Meta:
        ordering = ('-timestamp',)
