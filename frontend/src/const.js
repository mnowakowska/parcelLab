// Backend API
export const ORDERS_API = '/api/order/';
export const ORDERS_URL = `${process.env.REACT_APP_BACKEND_API}${ORDERS_API}`;
