import React from 'react';
import { Card, Grid, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

import Article from './Article'


const useStyles = makeStyles(() => ({
  root: {
    padding: '1rem',
    marginTop: '2rem',
  },
  label: {
    fontSize: '0.8rem',
  }
}));


function Articles ({ articles }) {
  const classes = useStyles();

  return (
    <Card variant="outlined" className={classes.root}>
      <Typography className={classes.label} color='textSecondary'>
        Articles
      </Typography>
      <Grid>
        {articles.map((article, index) => (
          <Article key={index} article={ article } />))}
      </Grid>
    </Card>
  );
}

export default Articles;
