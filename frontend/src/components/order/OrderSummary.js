import React, { useCallback } from 'react';
import { Card, Grid } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { useHistory } from 'react-router-dom';

import Address from '../tracking/Address'
import InfoBox from '../InfoBox'


const useStyles = makeStyles(() => ({
  root: {
    padding: '1rem',
    marginTop: '2rem',
    cursor: 'pointer',
  },
}));


function OrderSummary ({ order }) {
  const classes = useStyles();
  const history = useHistory();

  const handleOnClick = useCallback(() => {
    history.push(`/order/${order.id}`)
  }, [order.id, history]);

  return (
    <Card variant="outlined" className={classes.root} onClick={handleOnClick}>
      <Grid
        container
        direction='row'
        justifyContent='space-between'
        alignItems='center'
      >
        <InfoBox label='Order Number' description={ order.order_no } />
        <InfoBox label='Current Status' description={ order.newest_status } />
        <Address address={order.address} />
      </Grid>
    </Card>
  );
}

export default OrderSummary;
