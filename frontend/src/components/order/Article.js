import React from 'react';
import { Grid, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(() => ({
  root: {
    marginTop: '1rem',
    width: '100%'
  },
  name: {
    fontSize: '1.2rem',
  },
  label: {
    fontSize: '0.8rem',
  },
  quantity: {
    fontSize: '1rem',
    marginLeft: '0.3rem'
  },
  image: {
    width: '5rem',
    margin: '0 2rem'
  }
}));


function Article ({ article: { article_quantity, image_url, name, article_no }}) {
  const classes = useStyles();
  return (
    <Grid container direction='row' alignItems='center' className={classes.root}>
      <Typography className={classes.quantity} color='textSecondary'>
        x {article_quantity}
      </Typography>
      <img src={image_url} alt={name} className={classes.image}/>
      <Grid item>
        <Grid container direction='column' justifyContent='space-between'>
          <Typography className={classes.name}>
            {name}
          </Typography>
          <Typography className={classes.label} color='textSecondary'>
            {article_no}
          </Typography>
        </Grid>
      </Grid>
    </Grid>
  );
}

export default Article;
