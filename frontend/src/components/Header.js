import React from 'react';
import { Link } from 'react-router-dom';
import { Grid, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import HomeIcon from '@material-ui/icons/Home';

const useStyles = makeStyles(() => ({
  root: {
    color: 'white',
    padding: '3rem 0',
  },
  header: {
    fontSize: '2.5rem',
    fontWeight: 700,
    textTransform: 'capitalize'
  },
  link: {
    color: 'white',
  }
}));


function Header () {
  const classes = useStyles();
  return (
    <Grid
      container
      direction='row'
      justifyContent='space-between'
      alignItems='center'
      className={classes.root}
    >
      <Typography variant='h1' className={classes.header} >Order status plugin</Typography>
      <Link to="/" className={classes.link}><HomeIcon /></Link>
    </Grid>
  );
}

export default Header;
