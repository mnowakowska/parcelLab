import React from 'react';
import { Grid, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(() => ({
  description: {
    fontSize: '1rem',
    fontWeight: 700,
  },
  label: {
    fontSize: '0.8rem',
  }
}));


function InfoBox ({ label, description }) {
  const classes = useStyles();

  return (
    <Grid item>
      <Typography className={classes.label} color='textSecondary'>
        { label }
      </Typography>
      <Grid className={classes.description}>
        { description }
      </Grid>
    </Grid>
  );
}

export default InfoBox;
