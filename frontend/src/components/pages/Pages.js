import React, { Suspense } from 'react';
import { Switch, Route } from 'react-router-dom';
import { CircularProgress, Container } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

import DashboardPage from './DashboardPage.Lazy';
import HomePage from './HomePage.Lazy';
import OrderDetailsPage from './OrderDetailsPage.Lazy';
import Header from '../Header';
import { UserProvider } from '../userContext';


const useStyles = makeStyles(() => ({
  spinner: {
    position: 'absolute',
    top: '50%',
    left: '50%',
  },
}));


function Pages () {
  const classes = useStyles();

  return (
    <Container maxWidth="sm">
      <Header />
      <Suspense fallback={<CircularProgress className={classes.spinner} />}>
      <UserProvider>
        <Switch>
          <Route path="/dashboard">
            <DashboardPage />
          </Route>
          <Route path="/order/:id">
            <OrderDetailsPage />
          </Route>
          <Route exact path="/">
            <HomePage />
          </Route>
        </Switch>
      </UserProvider>
      </Suspense>
    </Container>
  );
}

export default Pages;
