import React, { useContext, useEffect, useState } from 'react';
import axios from 'axios';
import { Paper, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

import OrderSummary from '../order/OrderSummary'
import { UserContext } from '../userContext';
import { ORDERS_URL } from "../../const";


const useStyles = makeStyles(() => ({
  root: {
    padding: '1rem',
    marginBottom: '2rem'
  },
}));


function DashboardPage () {
  const classes = useStyles();
  const [orders, setOrders] = useState([]);
  const { user } = useContext(UserContext);

  useEffect(() => {
    const getOrders = async () => {
      const response = await axios.get(ORDERS_URL + `?user=${user}`);
      setOrders(response.data);
    }
    getOrders();
  }, [user, setOrders]);


  return (
    <Paper className={classes.root}>
      <Typography>
        Your Orders
      </Typography>
      {
        orders.length ?
          orders.map(order => <OrderSummary key={order.id} order={order} />) :
          'No orders'
      }
    </Paper>
  );
}

export default DashboardPage;
