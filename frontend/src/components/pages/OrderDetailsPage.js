import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import axios from 'axios';
import { Paper } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

import InfoBox from '../InfoBox'
import Articles from '../order/Articles';
import Address from '../tracking/Address';
import Tracking from '../tracking/Tracking'
import { ORDERS_URL } from '../../const';


const useStyles = makeStyles(() => ({
  root: {
    padding: '1rem',
    marginBottom: '2rem'
  },
}));


function OrderDetailsPage () {
  const classes = useStyles();
  let { id } = useParams();
  const [order, setOrder] = useState(null);

  useEffect(() => {
    const getOrder = async () => {
      const response = await axios.get(`${ORDERS_URL}${id}`);
      setOrder(response.data);
    }
    getOrder();
  }, [id, setOrder]);


  return order && (
    <Paper className={classes.root}>
      <InfoBox label='Order Number' description={ order.order_no } />
      <Address address={order.address} />
      {order.tracking.map((tracking, index) => (
        <Tracking tracking={ tracking } key={ index }/>))}
      { !!order.articles.length && <Articles articles={order.articles} /> }
    </Paper>
  );
}

export default OrderDetailsPage;
