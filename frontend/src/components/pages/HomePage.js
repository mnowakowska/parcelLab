import React, { useCallback, useContext, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { Button, TextField, Paper, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

import { UserContext } from '../userContext';

const useStyles = makeStyles(() => ({
  root: {
    padding: '3rem',
    marginBottom: '3rem'
  },
  input: {
    width: '100%',
    margin: '1rem 0 2rem'
  },
  button: {
    width: '100%'
  }
}));


function Home () {
  const classes = useStyles();
  let history = useHistory();
  const { setUser } = useContext(UserContext);
  const [email, setEmail] = useState('');

  const handleChange =  useCallback((event) => {
    setEmail(event.target.value);
  }, []);

  const handleOnSubmit = useCallback(() => {
    setUser(email);
    history.push('/dashboard');
  }, [email, setUser, history]);

  return (
    <Paper className={classes.root}>
      <Typography>
        Please enter your email address to see your recent orders
      </Typography>
      <form onSubmit={handleOnSubmit}>
        <TextField
          label='Email'
          className={classes.input}
          value={email}
          onChange={handleChange}
          InputLabelProps={{
            shrink: true,
          }}
          id='email'
          required
        />
        <Button className={classes.button} variant="contained" color="primary" type='submit' >
          Send
        </Button>
      </form>
    </Paper>
  );
}

export default Home;
