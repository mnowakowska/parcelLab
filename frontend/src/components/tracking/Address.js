import React from 'react';
import { Grid, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(() => ({
  root: {
    marginTop: '1rem',
    width: '100%'
  },
  address: {
    fontSize: '1rem',
    fontWeight: 700,
  },
  label: {
    fontSize: '0.8rem',
  }
}));


function Address ({address: { street, zip_code, city }}) {
  const classes = useStyles();
  return (
    <Grid item className={classes.root}>
      <Typography className={classes.label} color='textSecondary'>
        Delivery Address
      </Typography>
      <Grid container direction='column'>
        <Typography className={classes.address}>
          { street }
        </Typography>
        <Typography className={classes.address}>
          { zip_code } {city}
        </Typography>
      </Grid>
    </Grid>
  );
}

export default Address;
