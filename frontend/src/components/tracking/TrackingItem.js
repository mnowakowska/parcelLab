import React from 'react';
import { Grid, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import {
  TimelineConnector, TimelineContent, TimelineDot, TimelineItem,
  TimelineSeparator, TimelineOppositeContent } from '@material-ui/lab';


const useStyles = makeStyles(() => ({
  date: {
    fontSize: '0.9rem',
    width: '100%'
  },
  label: {
    fontSize: '1rem',
    fontWeight: 700,
  },
  statusSection: {
    flex: 2,
  },
  description: {
    fontSize: '0.8rem',
  },
}));


function TrackingItem ({ timestamp, status: { status_text, status_details } }) {
  const classes = useStyles();

  return (
    <TimelineItem>
      <TimelineOppositeContent>
        <Typography color="textSecondary" className={classes.date}>{ timestamp }</Typography>
      </TimelineOppositeContent>
      <TimelineSeparator>
        <TimelineDot />
        <TimelineConnector />
      </TimelineSeparator>
      <TimelineContent className={classes.statusSection} >
        <Grid container direction='column'>
          <Typography className={classes.label}>{ status_text }</Typography>
          <Typography className={classes.description}>{ status_details }</Typography>
        </Grid>
      </TimelineContent>
      </TimelineItem>
  );
}

export default TrackingItem;
