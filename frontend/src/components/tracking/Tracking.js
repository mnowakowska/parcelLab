import React from 'react';
import { Card } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import Timeline from '@material-ui/lab/Timeline';

import InfoBox from '../InfoBox'
import TrackingItem from './TrackingItem'


const useStyles = makeStyles(() => ({
  root: {
    padding: '1rem',
    marginTop: '2rem',
  },
  timeline: {
    padding: 0,
  }
}));


function Tracking ({ tracking: { tracking_number, tracking_checkpoints } }) {
  const classes = useStyles();

  return (
    <Card variant="outlined" className={classes.root}>
      <InfoBox label='Tracking Number' description={ tracking_number } />
      <Timeline className={classes.timeline}>
        {tracking_checkpoints.map((checkpoint, index) => (
          <TrackingItem key={index} timestamp={checkpoint.timestamp} status={checkpoint.status}/>))}
      </Timeline>
    </Card>
  );
}

export default Tracking;
