const theme = {
  palette: {
      type: 'light',
      primary: {
        main: '#021541',
      },
      background: {
        paper: 'hsl(0, 0%, 98%)',
        default: 'hsl(236, 33%, 92%)'
      }
    },
    typography: {
      fontFamily: "Josefin Sans, sans-serif",
      fontSize: 18
    }
}

export default theme;
