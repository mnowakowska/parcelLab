import React from 'react';
import { render, screen } from '@testing-library/react';

import HomePage from '../components/pages/HomePage';

test('renders form with email', () => {
  const root = render(<HomePage />);
  expect(screen.getByText(/Please enter your email address/i)).toBeInTheDocument();
  expect(root).toMatchSnapshot();
});
