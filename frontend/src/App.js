import React from 'react'
import CssBaseline from '@material-ui/core/CssBaseline';
import { createTheme, makeStyles, ThemeProvider } from '@material-ui/core/styles';
import { BrowserRouter as Router } from 'react-router-dom';

import theme from './theme';

import Background from './images/federico-beccari-L8126OwlroY-unsplash.jpg';
import Pages from './components/pages/Pages';

const useStyles = makeStyles(() => ({
  root: {
    backgroundImage: `url(${Background})`,
    backgroundSize: '100% 300px',
    backgroundRepeat: 'no-repeat',
    minHeight: '300px',
  },
}));

function App() {
  const classes = useStyles();

  return (
    <Router>
      <ThemeProvider theme={createTheme(theme)}>
        <CssBaseline />
        <div className={classes.root}>
          <Pages />
        </div>
      </ThemeProvider>
    </Router>
  );
}

export default App;
