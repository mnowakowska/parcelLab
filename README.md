# order status plugin

## To run the application
In order to run the application you need to install docker and docker compose

## Setting up

1. Clone the repository
2. Run the application:
```bash
docker-compose build && docker-compose up
```

To load sample data to database run:
```bash
docker-compose run backend python3 manage.py load_orders --file data/trackings.csv
docker-compose run backend python3 manage.py load_checkpoints --file data/checkpoints.csv
```

Backend: http://localhost:8000/api/
Frontend: http://localhost:3001


## Tests

```bash
docker-compose run backend pytest
docker-compose run frontend yarn test --watchAll=false
```
